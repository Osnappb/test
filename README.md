It is about the service that is provided by photo booth company. Nowadays, wedding couple wants best responsive service with number of email exchanges of various small items.
as a company you have to be able to respond to their queries on time and promptly.

---

## Template of print

This is very important whether is 4x6 or 2x6. 

1. 4X6 is available is various templat formats.
2. it can be customized with color, font, design, and background. 
3. same way, 2x6 cab be customized 
4. 2X6 print copy is equally important to wedding client. 

---

## Photographs Quality

they want the sharpest quality yet fun photographs

1. image sharness is extremely important.
2. we use canon 5d mark III camera to get the best result. 
3. we produced the sharpest quality pictures. 
4. color ton or white balace is perfect. 
5. best quality photos out there by our photo booth. We know that they are very sharp. Not many boothers in industry offer this professinal camera.
6. most of them have amature version of DSLR plugged to their photo booth, this may not produce "wow" quality photographs. so, this makes it big different between us and other boothers out there in market. 

---

## Backdrops 

people do not understand but backdrops are the presentation for you, it makes people say "WOW" what a set up or oh! that looks really good.

1. we have all kinds of sequin backdrops.
2. also, we provide rose petal kinds of backdrop as well as size of 8X10. 
3. we also offer the most interactive double sided sequin backdrops that will change color by swipping your hands.
4. we have gold to black and vice versa in double sided sequin backdrop. this is our best and most famous backdrop.
5. [Photo Booth Rental](https://www.osnapphotobooth.com/blog)
6. [open air photo booth](https://www.osnapphotobooth.com/)

come visit us and see what we offer, you may like us and you may book us. see you out there! 